#Coding Journal - Week 4
##10005227 - Jason Wallace

###Week 4 was a bit easier than Week 3, although in some areas I was still unsure. I understood the mvc concept, although really all I did was copy and paste some items. I do not understand how to create a view. Also, I did not get indexes, since I did not know what to alter and consequently, did not alter the indexes. I do not know what else to say, since it felt like a relatively short tutorial. This is what I did not understand:

###The “view” files need to match a method in the controller, so that they can be injected into the _Layout.cshtml when you see the @RenderBody() syntax.